package scannerassignmnet;

import java.util.*;




public class Sum {

	public static void main(String[] args) {
		Scanner in =new Scanner(System.in);
		int arr[]= new int[5];
		
		long sum=0, max=Long.MIN_VALUE,min=Long.MAX_VALUE;
		System.out.println("enter the numbers");
		for(int i=0;i<5;i++) {
			long n=in.nextLong();
			sum+=n;
			max=Math.max(max, n);
			min=Math.min(min, n);
		}
		System.out.println((sum-max)+"  "+(sum-min));
		

	}

}
